#! $(which awk) -f

# Ignore '#' comment line
/^#/ { next }
# Match each operation and a wild card

/\+=/ {
    print "Detected += Operation"
    # $1 Location
    # $2 +=
    # $3 Value
    locations[$1] += $3
}

/\-=/ {
    print "Detected -= Operation"
    # $1 Location
    # $2 -=
    # $3 Value
    locations[$1] -= $3
}

/\/=/ {
    print "Detected /= Operation"
    # $1 Location
    # $2 /=
    # $3 Value
    locations[$1] -= $3
}

/\*=/ { print "Detected *= Operation"

}

/=/ {
    print "Detected = Operation"
    # $1 Location
    # $2 =
    # $3 Value
    locations[$1] = $3 # Assignment executed
}

END {
    for (location in locations) {
	print location ": " locations[location]
    }
}
